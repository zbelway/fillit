/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 23:26:48 by zbelway           #+#    #+#             */
/*   Updated: 2016/02/18 21:29:31 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include <stdlib.h>
# include <fcntl.h>
# include "libft/libft.h"

# define STDIN	0
# define STDOUT	1
# define STDERR	2

typedef	struct	s_board
{
	int			matrix;
	char		**board;
	int			n_tets;
}				t_board;

typedef struct	s_tetr
{
	char		**block;
	int			i;
	int			j;
	int			num;
	int			i_max;
	int			j_max;
}				t_tetr;

int				traverse_neighbors(char **block);
int				solve(t_tetr *tets[], t_board *b);
void			print_arr(char **arr, int m, int n, int size);
void			ft_error(void);
int				count_tet(char *tet);
int				check_buf(char *buf);
int				read_and_check_file(int fd, t_tetr *tet_arr[]);
t_tetr			*input_tet(char *buf, char c, t_tetr *ret);
t_tetr			*new_tetr(char *buf, int i, int is_tet);
void			shift_tet_to_left_corner(char **tet, t_tetr *ret);
t_tetr			**try_backtrack(t_tetr *tets[], t_board *b);
void			fillit(t_tetr *tets[], t_board *b, int matrix);
int				try_place_tet(t_tetr *tets[], t_board b);
void			place_tet(t_tetr *tets[], t_board b);

#endif
