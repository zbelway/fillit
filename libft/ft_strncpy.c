/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 19:05:47 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/23 15:32:55 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strncpy(char *dup, const char *str, size_t n)
{
	size_t	i;

	i = 0;
	while (str[i] && i < n)
	{
		dup[i] = str[i];
		i++;
	}
	while (i < n)
		dup[i++] = '\0';
	return (dup);
}
