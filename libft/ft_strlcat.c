/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 17:09:12 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/23 15:59:48 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_strlcat(char *dest, const char *str, size_t size)
{
	size_t		n;
	size_t		len;
	char		*s1;
	const char	*s2;

	n = size;
	s1 = dest;
	s2 = str;
	while (n-- > 0 && *s1)
		s1++;
	len = s1 - dest;
	if ((n = size - len) == 0)
		return (len + ft_strlen(str));
	while (*s2)
	{
		if (n > 1)
		{
			*s1++ = *s2;
			n--;
		}
		s2++;
	}
	*s1 = '\0';
	return (len + (s2 - str));
}
