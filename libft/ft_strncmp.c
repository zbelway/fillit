/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/30 16:52:40 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/24 14:50:15 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int					ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t			i;
	unsigned char	*s1temp;
	unsigned char	*s2temp;

	i = 0;
	s1temp = (unsigned char *)s1;
	s2temp = (unsigned char *)s2;
	while (i < n && (s1temp[i] || s2temp[i]))
	{
		if (s1temp[i] != s2temp[i])
			return (s1temp[i] - s2temp[i]);
		i++;
	}
	return (0);
}
