/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 16:10:34 by zbelway           #+#    #+#             */
/*   Updated: 2016/02/18 19:00:23 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

static int	find_starting_m(int n_tets)
{
	int		i;

	if (n_tets > 26)
		ft_error();
	i = 0;
	if (n_tets == 1)
		return (2);
	while (i * i < n_tets * 4)
		i++;
	return (i);
}

static void	print_usage(void)
{
	ft_putstr("usage: ./fillit file\n");
}

int			main(int ac, char *av[])
{
	int		fd;
	t_tetr	*tet_arr[16];
	t_board	b;

	if (ac == 2)
	{
		if ((fd = open(av[1], O_RDONLY)) > -1)
		{
			b.n_tets = read_and_check_file(fd, tet_arr);
			fillit(tet_arr, &b, find_starting_m(b.n_tets));
			free(b.board);
		}
		else
			ft_error();
	}
	else
		print_usage();
	return (0);
}
