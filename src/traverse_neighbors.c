/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   traverse_neighbors.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/18 15:35:19 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/18 20:54:35 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

static int	try_neighbors(char **block, int i, int j)
{
	int		sum;

	sum = 0;
	if (i > 0 && block[i - 1][j] != '.')
		sum++;
	if (j > 0 && block[i][j - 1] != '.')
		sum++;
	if (i < 3 && block[i + 1][j] != '.')
		sum++;
	if (j < 3 && block[i][j + 1] != '.')
		sum++;
	return (sum);
}

int			traverse_neighbors(char **tet)
{
	int		ret;
	int		i;
	int		j;

	i = 0;
	ret = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (tet[i][j] != '.')
				ret += try_neighbors(tet, i, j);
			j++;
		}
		i++;
	}
	if (ret == 6 || ret == 8)
		return (1);
	return (0);
}
