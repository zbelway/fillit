/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_tetr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 18:41:03 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/18 18:58:31 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

t_tetr	*new_tetr(char *buf, int i, int is_tet)
{
	t_tetr	*ret;

	ret = malloc(sizeof(t_tetr));
	if (ret)
	{
		if (is_tet)
			ret = input_tet(buf, (char)(i + 65), ret);
		ret->i = 0;
		ret->j = 0;
		ret->num = i + 1;
	}
	else
		ret = NULL;
	return (ret);
}
