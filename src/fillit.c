/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 17:47:34 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/18 16:27:01 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

int				solve(t_tetr *tets[], t_board *b)
{
	if (try_place_tet(tets, *b) == 0)
		return (0);
	else
	{
		place_tet(tets, *b);
		tets++;
		if ((*tets)->num == b->n_tets + 1)
			return (1);
		else
		{
			(*tets)->i = 0;
			(*tets)->j = 0;
			if (solve(tets, b))
				return (1);
			else
			{
				tets = try_backtrack(tets, b);
				if ((*tets)->num == 1 && (*tets)->i == 0 && (*tets)->j == 0)
					return (0);
				else
					return (solve(tets, b));
			}
		}
	}
}

static char		**new_board(int size)
{
	char		**ret;
	int			i;

	i = 0;
	ret = (char **)malloc(sizeof(char *) * (size));
	if (ret)
	{
		while (i < size)
		{
			ret[i] = ft_strnew(size);
			i++;
		}
	}
	else
		ret = NULL;
	return (ret);
}

void			fillit(t_tetr *tets[], t_board *b, int matrix)
{
	(*tets)->i = 0;
	(*tets)->j = 0;
	b->matrix = matrix;
	b->board = new_board(78);
	while (b->matrix < 78)
	{
		if (solve(tets, b))
			break ;
		(*tets)->i = 0;
		(*tets)->j = 0;
		b->matrix += 1;
	}
	print_arr(b->board, 0, 0, b->matrix);
}
