/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_and_check_file.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 18:44:13 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/18 18:58:41 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

int			read_and_check_file(int fd, t_tetr *tet_arr[])
{
	char	buf[21];
	int		ret;
	int		i;

	i = 0;
	while ((ret = read(fd, buf, 21)))
	{
		buf[ret] = '\0';
		if (count_tet(buf))
		{
			tet_arr[i] = new_tetr(buf, i, 1);
			i++;
		}
		else
			ft_error();
	}
	if (buf[20] != '\0')
		ft_error();
	tet_arr[i] = new_tetr(ft_strnew(21), i, 0);
	return (i);
}
