/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_tet.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 16:45:20 by zbelway           #+#    #+#             */
/*   Updated: 2016/02/18 18:57:12 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

int		count_tet(char *tet)
{
	int	i;
	int	full;

	i = 0;
	full = 0;
	while (i < 20)
	{
		if (i % 5 == 4 && tet[i] != '\n')
			return (0);
		if (i % 5 != 4 && tet[i] != '.' && tet[i] != '#')
			return (0);
		if (tet[i] == '#')
			full++;
		i++;
	}
	if (tet[i] != '\0' && tet[i] != '\n')
		return (0);
	if (full != 4)
		return (0);
	return (1);
}
