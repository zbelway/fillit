/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shift_tet_to_left_corner.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 22:17:57 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/18 18:59:54 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

static int		find_leftmost(char **tet)
{
	int			i;
	int			j;

	j = 0;
	while (j < 4)
	{
		i = 0;
		while (i < 4)
		{
			if (tet[i][j] != '.')
				return (j);
			i++;
		}
		j++;
	}
	return (i);
}

static int		find_topmost(char **tet)
{
	int			i;
	int			j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (tet[i][j] != '.')
				return (i);
			j++;
		}
		i++;
	}
	return (j);
}

void			shift_tet_to_left_corner(char **tet, t_tetr *ret)
{
	int			i;
	int			j;
	int			m;
	int			n;

	m = 0;
	j = find_leftmost(tet);
	i = find_topmost(tet);
	ret->i_max -= i;
	ret->j_max -= j;
	while (m < 4)
	{
		n = 0;
		while (n < 4)
		{
			if (j + n < 4 && m + i < 4)
				tet[m][n] = tet[m + i][n + j];
			else
				tet[m][n] = '.';
			n++;
		}
		m++;
	}
	ret->block = tet;
}
