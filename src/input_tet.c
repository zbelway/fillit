/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_tet.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 22:05:49 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/22 15:17:49 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

static int		valid_neighbors(char *buf, int i)
{
	char		*block;
	int			top;
	int			left;
	int			right;
	int			bottom;

	block = buf;
	if (i == 0)
		top = 0;
	else
		top = (*(block - 5) == '#');
	if (i == 4)
		bottom = 0;
	else
		bottom = (*(block + 5) == '#');
	left = (*(block - 1) == '#' || *(block - 1) == '\n');
	right = (*(block + 1) == '#' || *(block + 1) == '\n');
	if ((top + bottom + left + right) > 0)
		return (1);
	return (0);
}

static int		check_block_part(char *buf, int ij[], char c, char **tet)
{
	int			is_valid;

	is_valid = valid_neighbors(buf, ij[0]);
	if (is_valid)
	{
		tet[ij[0]][ij[1]] = c;
		return (1);
	}
	else
		ft_error();
	return (0);
}

static void		update_ij_max(int ij[], int ij_max[])
{
	if (ij[0] > ij_max[0])
		ij_max[0] = ij[0];
	if (ij[1] > ij_max[1])
		ij_max[1] = ij[1];
}

static t_tetr	*return_correct_tetr(t_tetr *ret, int ij_max[],
		char **tet)
{
	ret->i_max = ij_max[0];
	ret->j_max = ij_max[1];
	if (traverse_neighbors(tet))
		shift_tet_to_left_corner(tet, ret);
	else
		ft_error();
	return (ret);
}

t_tetr			*input_tet(char *buf, char c, t_tetr *ret)
{
	char		**tet;
	int			ij[2];
	int			ij_max[2];

	ij_max[0] = 0;
	ij_max[1] = 0;
	ij[0] = 0;
	tet = (char **)malloc(sizeof(char *) * 4);
	if (tet)
		while (ij[0] < 4)
		{
			tet[ij[0]] = (char *)malloc(sizeof(char) * 4);
			ij[1] = 0;
			while (ij[1] < 5)
			{
				if (*buf == '#' && check_block_part(buf, ij, c, tet))
					update_ij_max(ij, ij_max);
				else if (*buf == '.')
					tet[ij[0]][ij[1]] = '.';
				buf++;
				ij[1] += 1;
			}
			ij[0] += 1;
		}
	return (return_correct_tetr(ret, ij_max, tet));
}
