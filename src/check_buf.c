/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_buf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 17:51:17 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/08 12:31:40 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

int			check_buf(char *buf)
{
	char	*tet;
	int		i;
	int		j;

	i = 0;
	tet = ft_strnew(21);
	tet[21] = '\0';
	while (buf[i])
	{
		j = 0;
		while (tet[j])
		{
			tet[j] = buf[i + j];
			j++;
		}
	}
	i = 0;
	tet = ft_strnew(20);
	while (buf[i])
	{
		i += 21;
	}
	return (1);
}
