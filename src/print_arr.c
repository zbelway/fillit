/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_arr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 19:33:01 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/10 20:03:16 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

void	print_arr(char **char_arr, int m, int n, int size)
{
	int	i;
	int	j;

	i = m;
	while (i < size + m)
	{
		j = n;
		while (j < size + n)
		{
			ft_putchar(char_arr[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}
