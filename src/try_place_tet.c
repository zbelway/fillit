/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   try_place_tet.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/09 18:21:07 by lsaussy           #+#    #+#             */
/*   Updated: 2016/02/17 18:18:54 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

static int		try_place_single_tet(t_tetr *tets[], t_board b)
{
	int			i;
	int			j;

	i = (*tets)->i;
	if (i + (*tets)->i_max >= b.matrix)
		return (2);
	if ((*tets)->j + (*tets)->j_max >= b.matrix)
		return (3);
	while (i < (*tets)->i_max + (*tets)->i + 1)
	{
		j = (*tets)->j;
		while (j < (*tets)->j + (*tets)->j_max + 1)
		{
			if (((*tets)->block)[i - (*tets)->i][j - (*tets)->j] != '.')
			{
				if (b.board[i][j] != '.')
					return (0);
			}
			j++;
		}
		i++;
	}
	return (1);
}

int				try_place_tet(t_tetr *tets[], t_board b)
{
	int			placed;

	placed = try_place_single_tet(tets, b);
	while (placed != 1)
	{
		if (placed == 2)
			return (0);
		else if (placed == 3)
		{
			(*tets)->j = 0;
			(*tets)->i += 1;
		}
		else if (placed == 0)
			(*tets)->j += 1;
		placed = try_place_single_tet(tets, b);
	}
	return (1);
}

void			place_tet(t_tetr *tets[], t_board b)
{
	int			i;
	int			j;

	i = (*tets)->i;
	while (i < b.matrix && i < (*tets)->i + (*tets)->i_max + 1)
	{
		j = (*tets)->j;
		while (j < b.matrix && j < (*tets)->j + (*tets)->j_max + 1)
		{
			if (((*tets)->block)[i - (*tets)->i][j - (*tets)->j] != '.')
				b.board[i][j] =
					((*tets)->block)[i - (*tets)->i][j - (*tets)->j];
			j++;
		}
		i++;
	}
}
