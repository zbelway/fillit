/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   try_backtrack.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 21:45:14 by zbelway           #+#    #+#             */
/*   Updated: 2016/02/17 18:15:36 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../fillit.h"

static void	clear_tet_block(t_board b, char c, t_tetr *tets)
{
	int		i;
	int		j;

	i = tets->i;
	while (i < tets->i_max + tets->i + 1)
	{
		j = tets->j;
		while (j < tets->j_max + tets->j + 1)
		{
			if (b.board[i][j] == c)
				b.board[i][j] = '.';
			j++;
		}
		i++;
	}
}

t_tetr		**try_backtrack(t_tetr *tets[], t_board *b)
{
	char	c;
	int		j;

	j = 0;
	tets--;
	while (((*tets)->block)[0][j] == '.')
		j++;
	c = ((*tets)->block)[0][j];
	clear_tet_block(*b, c, *tets);
	(*tets)->j += 1;
	if (((*tets)->num == 1) && ((*tets)->i + (*tets)->i_max + 1 >= b->matrix)
			&& ((*tets)->j + (*tets)->j_max + 1 >= b->matrix))
	{
		(*tets)->i = 0;
		(*tets)->j = 0;
	}
	return (tets);
}
