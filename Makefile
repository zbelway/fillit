# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lsaussy <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/04 19:29:27 by lsaussy           #+#    #+#              #
#    Updated: 2016/02/23 16:11:00 by lsaussy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror
LIBA = libft/libft.a
SRC = count_tet.c \
		ft_error.c \
		check_buf.c \
		main.c \
		read_and_check_file.c \
		new_tetr.c input_tet.c \
		print_arr.c \
		shift_tet_to_left_corner.c \
		fillit.c \
		try_backtrack.c \
		try_place_tet.c \
		traverse_neighbors.c
SDIRS = $(patsubst %, src/%, $(SRC))
OBJ = $(SRC:.c=.o)
NAME = fillit

all: $(NAME)

$(NAME):
	@make reclean -C libft
	$(CC) -c $(SDIRS) $(CFLAGS)
	$(CC) $(CFLAGS) $(OBJ) $(LIBA) -o $(NAME)

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
